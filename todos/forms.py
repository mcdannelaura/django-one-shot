from django.forms import ModelForm
from .models import TodoList, TodoItem


class TodoListForm(ModelForm):
    class Meta:
        model = TodoList
        fields = ["name"]


class UpdateTodoListForm(ModelForm):
    class Meta:
        model = TodoList
        fields = ["name"]


class DeleteTodoList(ModelForm):
    class Meta:
        model = TodoList
        fields = ["name"]


class TodoItemForm(ModelForm):
    class Meta:
        model = TodoItem
        fields = ["task", "due_date", "is_completed", "list"]


class UpdateTodoItemForm(ModelForm):
    class Meta:
        model = TodoItem
        fields = ["task", "due_date", "is_completed", "list"]
